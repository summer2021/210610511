import socket
import sys
import pickle
import json
import select
import threading
import numpy as np
import mindquantum as mq
import mindspore.ops as ops
from mindquantum.gate import H, X, Y, RY, RX
from request import *
from concurrent.futures import ThreadPoolExecutor
from mindquantum.engine import circuit_generator
from mindquantum.ops import QubitOperator
from mindquantum.nn import generate_pqc_operator
from mindspore import Tensor
from mindspore import context

if __name__ == "__main__":
    config = {}
    with open("config.json", "r") as f:
        config = json.load(f)
    port = 0
    try:
        port = int(config["listen_port"])
    except:
        raise ValueError("The port character must be [0-9], please check your port input.")
    if port >= 0 and port <= 65535:
        pass
    else:
        raise ValueError("The port value must be [0-65535], please check your port input.")
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    #server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server.bind(("127.0.0.1", port))
    server.listen(128)
    server.setblocking(True)
    timeout = 10
    #epoll = select.epoll()
    #epoll.register(server.fileno(), select.EPOLLIN)
    fd_to_socket = {server.fileno():server,}
    pool = ThreadPoolExecutor(max_workers=config["max_thread"]) # thread pool
    simulater = {} # simulater dict in order to combine socket and pqc
    print("start...\n")

    while True:
        client,add = server.accept()
        print("%s is connect", add)
        data = client.recv(1024)
        parseReq(data, client, 3, fd_to_socket, simulater)
        '''
        for fd, event in epoll.poll(timeout):
            socket = fd_to_socket[fd]
            if socket == server:
                client,add = server.accept()
                print("%s is connect", add)
                client.setblocking(True)
                epoll.register(client, select.EPOLLIN)
                fd_to_socket[client.fileno()] = client
            elif event & select.EPOLLHUP:
                epoll.unregister(fd)
                fd_to_socket[fd].close()
                del fd_to_socket[fd]
            elif event & select.EPOLLIN:
                data = socket.recv(1024)
                pool.submit(parseReq,data, client, fd, fd_to_socket, simulater)
        '''
        
                
                


