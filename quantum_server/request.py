import socket
import sys
import pickle
import select
import json
import numpy as np
import mindquantum as mq
import mindspore.ops as ops
from mindquantum.gate import H, X, Y, RY, RX
from mindquantum.engine import circuit_generator
from mindquantum.ops import QubitOperator
from mindquantum.nn import generate_pqc_operator
from mindspore import Tensor
from mindspore import context
    
def recon_circuit(str_circuit):
    '''
    convert circuit string to Circuit class.
    '''
    cir = mq.Circuit()
    cirList = str_circuit.split("\n")
    for each in cirList:
        if each.count("<-:") > 0:
            each = each.replace("<-:", ",")
            each = each.replace("(", ".on(")
        elif each.count("|") > 0:
            each = each.replace("(","('")
            each = each.replace("|", "').on(")
        else:
            each = each.replace("(", ".on(")
        cir += eval(each)
    #print(cir)
    return cir

def recon_tensor(str_tensor):
    '''
    convert tensor string to Tensor class.
    '''
    tenList = str_tensor.split(";")
    ar = np.array(json.loads(tenList[1])).reshape(json.loads(tenList[0]))
    return Tensor(ar.astype(np.float32))

def recon_response(tensor):
    '''
    conver Tensor class into tensor string.
    '''
    re = str(tensor.size)+";"
    ele = tensor.asnumpy().reshape(tensor.size)
    for i in range(0, len(ele)):
        re += str(ele[i])
        if(i < len(ele) - 1):
            re += " "
    return re

def tokenCheck(data):
    return True

def parseReq(data_str, client, fd, fd_to_socket, simulater):
    if not data_str:
        print(str(fd)+" recv "+str(data_str))
        epoll.unregister(fd)
        fd_to_socket[fd].close()
        del fd_to_socket[fd]
        del simulater[fd]
        return
    try:
        data = pickle.loads(data_str)
        print("recv circuit:",str(data))
        if tokenCheck(data):
            client.send(pickle.dumps(fd))
            context.set_context(mode=context.GRAPH_MODE, device_target="CPU")
            pqc = generate_pqc_operator(data[1], data[2], recon_circuit(data[3]), data[4])
            simulater[fd] = pqc
        else:
            client.send(pickle.dumps(""))
    except:
        if isinstance(data_str, bytes):
            print("recv datas:",data_str)
            tList = str(data_str, encoding="utf-8").split("\n")
            encoder_data = recon_tensor(tList[1])
            ansatz_data = recon_tensor(tList[2])
            result = simulater[int(tList[0])](encoder_data, ansatz_data)
            sen_str = recon_response(result[0])
            sen_str += "\n"
            sen_str += recon_response(result[1])
            sen_str += "\n"
            sen_str += recon_response(result[2])
            sen_str += "\n"
            print("send:",sen_str)
            client.send(sen_str.encode("utf-8"))
            del simulater[fd]
        else:
            epoll.unregister(fd)
            fd_to_socket[fd].close()
            del fd_to_socket[fd]
            del simulater[fd]
        