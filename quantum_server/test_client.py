import numpy as np
import pickle
import mindquantum as mq
import mindspore.ops as ops
import mindspore.nn as nn
from mindquantum.gate import H, X, Y, RY, RX
from mindquantum.engine import circuit_generator
from mindquantum.ops import QubitOperator
from mindquantum.nn import generate_pqc_operator
from mindspore import Tensor
from mindspore import context

class Net(nn.Cell):
    '''
    test network
    '''
    def __init__(self,ip, port, token, encoder_names, ansatz_names, circuit , ham):
        super(Net, self).__init__()
        self.cloud = ops.Cloud(ip, port, token)
        self.cloud.init_circuit(encoder_names, ansatz_names, circuit , ham)

    def construct(self, encoder_data, ansatz_data):
        return self.cloud(encoder_data, ansatz_data)

class Grad(nn.Cell):
    '''
    test grad network
    '''
    def __init__(self, network):
        super(Grad, self).__init__()
        self.grad = ops.GradOperation(sens_param=False)
        self.network = network
    
    def construct(self, encoder_data, ansatz_data):
        gout = self.grad(self.network)(encoder_data, ansatz_data)
        return gout

@circuit_generator(2)
def encoder(qubits):
    RY('a') | qubits[0]
    RY('b') | qubits[1]

@circuit_generator(2)
def ansatz(qubits):
    X | (qubits[0],qubits[1])
    RX('p1') | qubits[0]
    RX('p2') | qubits[1]

ham = mq.Hamiltonian(QubitOperator('Z1'))
encoder_names = ['a', 'b']
ansatz_names = ['p1', 'p2']
context.set_context(mode=context.GRAPH_MODE, device_target="CPU")
encoder_data = Tensor(np.array([[0.1,0.2]]).astype(np.float32))
ansatz_data = Tensor(np.array([0.3,0.4]).astype(np.float32))

def test_singel():
    cloud = ops.Cloud("127.0.0.1","20000","abc")
    cloud.init_circuit(encoder_names, ansatz_names, encoder+ansatz, ham)
    measure_result, encoder_grad, ansatz_grad= cloud(encoder_data, ansatz_data)
    print('Encoder data:',encoder_data)
    print("Ansatz data:",ansatz_data)
    print('Measurement result: ', measure_result.asnumpy())
    print('Gradient of encoder parameters: ', encoder_grad.asnumpy())
    print('Gradient of ansatz parameters: ', ansatz_grad.asnumpy())
    
def test_net():
    cloud = Net("127.0.0.1","20000","abc",encoder_names, ansatz_names, encoder+ansatz, ham)
    output = cloud(encoder_data, ansatz_data)
    print("Network output:", output)

def test_grad_net():
    cloud = Net("127.0.0.1","20000","abc",encoder_names, ansatz_names, encoder+ansatz, ham)
    grad = Grad(cloud)
    dlist = grad(encoder_data, ansatz_data)
    print("dlist:", dlist)

test_grad_net()

