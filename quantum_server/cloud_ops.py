from mindspore.ops.primitive import PrimitiveWithInfer
from mindspore.ops.primitive import prim_attr_register
from mindspore._checkparam import Validator as validator
from mindspore.common import dtype as mstype
import numpy as np
import mindspore as ms


class Cloud(PrimitiveWithInfer):
    @prim_attr_register
    def __init__(self, ip, port):
        self.init_prim_io_names(
            inputs=['encoder_data', 'ansatz_data'],
            outputs=['results', 'encoder_gradient', 'ansatz_gradient'])

    def check_shape_size(self, encoder_data, ansatz_data):
        """check shape size"""
        if len(encoder_data) != 2:
            raise ValueError(
                "PQC input encoder_data should have dimension size \
equal to 2, but got {}.".format(len(encoder_data)))
        if len(ansatz_data) != 1:
            raise ValueError(
                "PQC input ansatz_data should have dimension size \
equal to 1, but got {}.".format(len(ansatz_data)))
        if encoder_data[1] != 2:
            raise ValueError(f'Input encoder_data length {encoder_data[1]} \
mismatch with circuit encoder parameter number {2}')
        if ansatz_data[0] != 2:
            raise ValueError(f'Input ansatz_data length {ansatz_data[1]} \
mismatch with circuit ansatz parameter number {2}')

    def infer_shape(self, encoder_data, ansatz_data):
        self.check_shape_size(encoder_data, ansatz_data)
        return [encoder_data[0], 1], [encoder_data[0], 1,
                                      2], [encoder_data[0], 1, 2]

    def infer_dtype(self, encoder_data, ansatz_data):
        args = {'encoder_data': encoder_data, 'ansatz_data': ansatz_data}
        validator.check_tensors_dtypes_same_and_valid(args, mstype.float_type,
                                                      self.name)
        return encoder_data, encoder_data, encoder_data


cloud_pqc = Cloud('123', '456')
encoder_data = ms.Tensor(np.array([[1, 2]]).astype(np.float32))
ansatz_data = ms.Tensor(np.array([3, 4]).astype(np.float32))
f, g1, g2 = cloud_pqc(encoder_data, ansatz_data)
