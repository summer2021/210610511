"""Generate bprop for cloud aware ops"""

from .. import operations as P
from ..operations import cloud_ops as C
from .grad_base import bprop_getters

@bprop_getters.register(C.Cloud)
def get_bprop_cloud(self):
    "Generate bprop for cloud quantum ops"
    op = C.Cloud(ip=self.ip, port=self.port, token=self.token)

    def bprop(encoder_data, ansatz_data, out, dout):
        re, encoder_grad, ansatz_grad = op(encoder_data, ansatz_data)
        return encoder_grad, ansatz_grad
        
    return bprop