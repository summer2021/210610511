from ..primitive import PrimitiveWithInfer, prim_attr_register
from mindspore._checkparam import Validator as validator
from mindspore.common import dtype as mstype
import pickle
import socket
import pickle

class Cloud(PrimitiveWithInfer):

    @prim_attr_register
    def __init__(self, ip, port, token):
        self.init_prim_io_names(
            inputs=['encoder_data', 'ansatz_data'],
            outputs=['results', 'encoder_gradient', 'ansatz_gradient'])
        self.ip = ip
        self.port = port
        self.token = token
        self.__check_ip_port()
        self.client = socket.socket(socket.AF_INET,socket.SOCK_STREAM)

    def init_circuit(self, encoder_names, ansatz_names, circuit , ham):
        try:
            self.client.connect((self.ip, int(self.port)))
        except:
            raise ValueError("If your internet is well, please check your ip and port input.")
        self.__check_token(self.token, encoder_names, ansatz_names, circuit , ham)

    def __check_ip_port(self):
        ip_str = self.ip.split(".")
        if len(ip_str) != 4:
            raise ValueError("The number of '.' in IP must be three, please check your IP address.")
        for each in ip_str:
            try:
                each = int(each)
            except:
                raise ValueError("The character in IP must be [1-9], please check your IP address.")
            if each >= 0 and each <= 255:
                pass
            else:
                raise ValueError("Each section in IP must be [0-255], please check your IP address.")
        if int(self.port) >= 0 and int(self.port) <= 65535:
            pass
        else:
            raise ValueError("The value of port input must be [0-65535], please check your port input.")

    def __check_token(self, token, encoder_names, ansatz_names, circuit , ham):
        '''
        check user token,init circuit and ham if token has been checked successfully.
        '''
        self.client.send(pickle.dumps([token,encoder_names, ansatz_names, str(circuit), ham]))
        result = self.client.recv(1024)
        if not result:
            raise ValueError("Token error, please check your token input.")
        else:
            self.add_prim_attr("number", str(pickle.loads(result)))

    def check_shape_size(self, encoder_data, ansatz_data):
        """check shape size"""
        if len(encoder_data) != 2:
            raise ValueError(
                "PQC input encoder_data should have dimension size \
equal to 2, but got {}.".format(len(encoder_data)))
        if len(ansatz_data) != 1:
            raise ValueError(
                "PQC input ansatz_data should have dimension size \
equal to 1, but got {}.".format(len(ansatz_data)))
        if encoder_data[1] != 2:
            raise ValueError(f'Input encoder_data length {encoder_data[1]} \
mismatch with circuit encoder parameter number {2}')
        if ansatz_data[0] != 2:
            raise ValueError(f'Input ansatz_data length {ansatz_data[1]} \
mismatch with circuit ansatz parameter number {2}')

    def infer_shape(self, encoder_data, ansatz_data):
        self.check_shape_size(encoder_data, ansatz_data)
        return [encoder_data[0], 1], [encoder_data[0], 1,
                                      2], [encoder_data[0], 1, 2]

    def infer_dtype(self, encoder_data, ansatz_data):
        args = {'encoder_data': encoder_data, 'ansatz_data': ansatz_data}
        validator.check_tensors_dtypes_same_and_valid(args, mstype.float_type,
                                                      self.name)
        return encoder_data, encoder_data, encoder_data
