
#include "backend/kernel_compiler/cpu/quantum/cloud_cpu_kernel.h"
#include "utils/ms_utils.h"
#include "runtime/device/cpu/cpu_device_address.h"


namespace mindspore{
namespace kernel{
    void CloudCPUKernel::InitKernel(const CNodePtr & kernel_node){
        MS_EXCEPTION_IF_NULL(kernel_node);
        this->encoder_shape = AnfAlgo::GetInputDeviceShape(kernel_node, 0);
        this->ansatz_shape = AnfAlgo::GetInputDeviceShape(kernel_node, 1);
        this->ip = AnfAlgo::GetNodeAttr<string>(kernel_node, "ip");
        this->port = AnfAlgo::GetNodeAttr<string>(kernel_node,"port");
        this->number = AnfAlgo::GetNodeAttr<string>(kernel_node,"number");

        //connect to the cloud service
        this->client = socket(AF_INET, SOCK_STREAM, 0);
        struct sockaddr_in ser_addr;
        memset(&ser_addr,0,sizeof(ser_addr));
        ser_addr.sin_family = AF_INET;
        ser_addr.sin_addr.s_addr = inet_addr((char *)this->ip.c_str());
        ser_addr.sin_port = htons(atoi((this->port).c_str()));
        int re = connect(this->client, (struct sockaddr *)&ser_addr,sizeof(struct sockaddr));
        if(re < 0){
            MS_LOG(EXCEPTION) << "connect to the server failed, please check your internet.";
        }
    }

    bool CloudCPUKernel::Launch(
        const std::vector<AddressPtr> &inputs,
        const std::vector<AddressPtr> &workspace,
        const std::vector<AddressPtr> &outputs){
            if(inputs.size() != 2 || outputs.size() != 3){
                MS_LOG(EXCEPTION) << "Cloud error input size.";
            }
            auto encoder_data = reinterpret_cast<float *>(inputs[0]->addr);
            auto ansatz_data = reinterpret_cast<float *>(inputs[1]->addr);
            auto output = reinterpret_cast<float *>(outputs[0]->addr);
            auto encoder_data_grad = reinterpret_cast<float *>(outputs[1]->addr);
            auto ansatz_data_grad = reinterpret_cast<float *>(outputs[2]->addr);
            MS_EXCEPTION_IF_NULL(encoder_data);
            MS_EXCEPTION_IF_NULL(ansatz_data);
            MS_EXCEPTION_IF_NULL(output);
            MS_EXCEPTION_IF_NULL(encoder_data_grad);
            MS_EXCEPTION_IF_NULL(ansatz_data_grad);

            string req = "";
            req += this->number;
            req +="\n";
            req += EncodeStr(encoder_data, this->encoder_shape);
            req += "\n";
            req += EncodeStr(ansatz_data, this->ansatz_shape);
            int re = send(this->client, req.c_str(), req.length(),0);
            std::cout<<"send bytes:"<<re<<std::endl;
            if(re <= 0){
                MS_LOG(EXCEPTION) <<"send to the server failed,please check your internet.";
            }
            char *buf = new char[1024];
            recv(this->client, buf, 1024, 0);
            string res = buf;
            std::cout<<"recv datas:"<<res<<std::endl;
            std::vector<std::string> resList = Split(res, '\n');
            for(int i = 0;i < 3;++i){
                int size = 0;
                std::vector<float> data;
                DecodeStr(size, data, resList[i]);
                if(i == 0){
                    std::cout<<"output_size:"<<size<<";";
                    for(int j = 0;j < size;++j){
                        output[j] = data[j];
                        std::cout<<output[j]<<" ";
                    }
                    std::cout<<std::endl;
                }
                else if(i == 1){
                    std::cout<<"encoder_size:"<<size<<";";
                    for(int j = 0;j < size;++j){
                        encoder_data_grad[j] = data[j];
                        std::cout<<encoder_data_grad[j]<<" ";
                    }
                    std::cout<<std::endl;
                }
                else{
                    std::cout<<"ansatz_size:"<<size<<";";
                    for(int j = 0;j < size;++j){
                        ansatz_data_grad[j] = data[j];
                        std::cout<<ansatz_data_grad[j]<<" ";
                    }
                    std::cout<<std::endl;
                }
            }
            return true;
        }

    //a spilt helper function in order to split response_data.
    std::vector<std::string> CloudCPUKernel::Split(std::string data, char character){
        std::vector<std::string> re;
        std::string start = "";
        std::cout<<"split str:"<<data<<"[";
        for(size_t i = 0;i < data.length();++i){
            if(data[i] == character){
                std::cout<<start<<",";
                re.push_back(start);
                start = "";
            }else{
                start += data[i];
                if(i == data.length() - 1){
                    std::cout<<start<<",";
                    re.push_back(start);
                    start = "";
                }
            }
        }
        std::cout<<"]"<<std::endl;
        return re;
    }
    //transpose encoder_data to string in order to send
    std::string CloudCPUKernel::EncodeStr(
    float *encoder, std::vector<size_t> encoder_shape){
        string re= "[";
        size_t sum = 1;
        for(size_t i = 0;i < encoder_shape.size();++i){
            re += std::to_string(SizeToInt(encoder_shape[i]));
            sum *= encoder_shape[i];
            if(i < encoder_shape.size() - 1){
                re += ",";
            }
        }
        re += "]";
        re += ";[";
        for(size_t i = 0;i < sum;++i){
            re += std::to_string(encoder[i]);
            if(i < sum - 1){
                re += ",";
            }
        }
        re += "]";
        return re;
    }
        
    void CloudCPUKernel::DecodeStr(int &size, std::vector<float> &datas, std::string res){
        std::vector<std::string> tem = Split(res, ';');
        size = atoi(tem[0].c_str());
        std::vector<std::string> arr = Split(tem[1],' ');
        for(std::vector<std::string>::iterator it = arr.begin(); it != arr.end();++it){
            datas.push_back(atof(it->c_str()));
        }
    }
}//namespace kernel
}//namespace mindspore