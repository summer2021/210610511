
#ifndef MINDSPORE_CCSRC_BACKEND_KERNEL_COMPILER_CPU_CLOUD_CPU_KERNEL_H_
#define MINDSPORE_CCSRC_BACKEND_KERNEL_COMPILER_CPU_CLOUD_CPU_KERNEL_H_

#include <vector>
#include <memory>
#include <string>
#include<unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include "backend/kernel_compiler/cpu/cpu_kernel.h"
#include "backend/kernel_compiler/cpu/cpu_kernel_factory.h"
#include "backend/kernel_compiler/cpu/quantum/quantum_simulator/pqc_simulator.h"
#include "backend/kernel_compiler/cpu/quantum/quantum_simulator/transformer.h"
#include "backend/kernel_compiler/cpu/quantum/quantum_simulator/circuit.h"
#include "backend/kernel_compiler/cpu/quantum/quantum_simulator/parameter_resolver.h"

namespace mindspore{
namespace kernel{
class CloudCPUKernel :  public CPUKernel{
    public:
        CloudCPUKernel() = default;
        ~CloudCPUKernel() override = default;

        void InitKernel(const CNodePtr & kernel_node) override;

        bool Launch(const std::vector<AddressPtr> &inputs,const std::vector<AddressPtr> &workspace,
        const std::vector<AddressPtr> &outputs) override;

        std::vector<std::string> Split(std::string data, char character);
        std::string EncodeStr(float *encoder, std::vector<size_t> encoder_shape);
        void DecodeStr(int &size, std::vector<float> &datas, std::string res);

    private:
        std::string ip;
        std::string port;
        std::string number;
        std::vector<size_t> encoder_shape;
        std::vector<size_t> ansatz_shape;
        int client;
};
MS_REG_CPU_KERNEL(Cloud,
    KernelAttr()
    .AddInputAttr(kNumberTypeFloat32)
    .AddInputAttr(kNumberTypeFloat32)
    .AddOutputAttr(kNumberTypeFloat32)
    .AddOutputAttr(kNumberTypeFloat32)
    .AddOutputAttr(kNumberTypeFloat32),
    CloudCPUKernel);
}//namespace kernel
}//namespace mindspore

#endif //MINDSPORE_CCSRC_BACKEND_KERNEL_COMPILER_CPU_CLOUD_CPU_KERNEL_H_